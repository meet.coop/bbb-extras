# bbb-extras
Install extra utils for BigBlueButton installs.

It includes:

## bbb-download
Generate MP4 files from bbb recordings. See https://github.com/unicode-it/bbb-download.git.
